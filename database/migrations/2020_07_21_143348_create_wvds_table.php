<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWvdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wvds', function (Blueprint $table) {
            $table->id();
            // $table->string('username')->nullable();
            $table->string('last_user')->nullable();
            $table->string('vm_name', 100);
            $table->string('resource_group', 100);
            $table->time('poweroff_at', 0);
            $table->time('poweron_at', 0);
            $table->json('daysofweek')->default(new \Illuminate\Database\Query\Expression('(JSON_ARRAY())'));
            $table->string('state');
            $table->boolean('autostart')->default(true);
            $table->string('timezone')->default('America/New_York');
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wvds');
    }
}
