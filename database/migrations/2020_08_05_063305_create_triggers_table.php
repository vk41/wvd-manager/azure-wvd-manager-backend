<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriggersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('triggers', function (Blueprint $table) {
            // $table->id();
            $table->uuid('uid');
            $table->string('vm_name', 100);
            $table->string('resource_group', 100);
            $table->string('action', 100)->comment('Poweroff or power down');
            $table->dateTime('trigger_at')->useCurrent()->comment('UTC time');
            $table->dateTime('notified_at')->nullable()->comment('UTC time');
            $table->dateTime('responded_at')->nullable()->comment('UTC time');
            $table->dateTime('completed_at')->nullable()->comment('UTC time');
            $table->string('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('triggers');
    }
}
