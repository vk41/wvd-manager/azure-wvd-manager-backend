<?php

return [
    'public_url' => 'https://beta.amailonline.com/',
    'tenant_id' => env('TENANT_ID'),
    'client_id' => env('CLIENT_ID'),
    'client_secret' => env('CLIENT_SECRET'),
    'subscription_id' => env('SUBSCRIPTION_ID'),
    'notification' => [
        'before' => 10, // minutes. Send x minutes before trigger time.
        'email' => true
    ],
    'reports' => [
        'vishnu@compusouk.com'
    ],    
];
