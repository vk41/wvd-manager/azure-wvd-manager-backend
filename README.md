
# Azure WVD Manager (auto deallocate/start)

## Features
- Automatically deallocate/start user WVD at specified time in the database. 
- Sends email 2 notifications 1 hour and 15 minutes before the shutdown. 
- Exposes a websocket connection to electron desktop app, to show popup notifications inside WVD.
- Exposes API for portal to manage WVDs.
## Todo
- Add security.
- UI for user self service to change timings.
- Impliment socialize
- Make mobile app to start/stop/restart WVD (PWA or corodva).

## Setup

Add below to env file.
```
TENANT_ID=<azure_tenent_id>
CLIENT_ID=<azure_client_id>
CLIENT_SECRET=<azure_api_client_secret>
SUBSCRIPTION_ID=<azure_subscription_id>

# Package uses pusher api, not pusher. Credentials are not real.

PUSHER_APP_ID=JmTxqLcscdtMcn6wJesxuLHdi2XkjTTW
PUSHER_APP_KEY=jbgx2SorpBx1ha4nWOMtoHMiExhwcANO
PUSHER_APP_SECRET=uwbUQ1NpdqqKSeEMt4ZKEO0plIGt6vAL
PUSHER_APP_CLUSTER=mt1
```
