{{-- @extends('layouts.app') --}}

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <table id="table" class="table-sm" data-toggle="table" data-search="true" data-sortable="true"
                        data-url="{{ route('azure.data') }}" data-pagination="true" data-page-size="20"
                        data-sort-name="status" data-sort-order="asc"
                        data-show-refresh="true">
                        <thead>
                            <tr>
                                <th data-field="status" data-sortable="true" data-sortable="true"
                                    data-formatter="virtualMachineStatus">Status</th>
                                <th data-field="virtualMachine" data-sortable="true">VM Name</th>
                                <th data-field="resourceGroup" data-sortable="true">Resource Group</th>
                                <th data-formatter="actionButtonFormatter" data-sortable="true">Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getVmStateIcon(status) {
        switch(status) {
            case 'running': return '<i class="fas fa-play text-success"></i>';break;
            case 'deallocated': return '<i class="fas fa-trash text-danger"></i>';break;
            case 'stopped': return '<i class="fas fa-stop text-warning"></i>';break;
            default: return ''; 
        }
    }

    function virtualMachineStatus(value, row, index) {
        var icon = getVmStateIcon(row.status);
        return [
            icon,
            value
        ].join(' ');
    }
    function actionButtonFormatter(value, row, index) {
        var btn = [`<button data-vm="${row.virtualMachine}" data-resource="${row.resourceGroup}" data-action="poweron" title="Power On" class="btn btn-sm btn-outline-success action-button">`,
                '<i class="fas fa-power-off"></i>',
                `</button>`].join('');

        if(row.status == 'running') {
            btn = [
                `<button data-vm="${row.virtualMachine}" data-resource="${row.resourceGroup}" data-action="poweroff" title="Power Off" class="btn btn-sm btn-outline-danger action-button">`,            
                '<i class="fas fa-power-off"></i>',
                '</button>',
                `<button data-vm="${row.virtualMachine}" data-resource="${row.resourceGroup}" data-action="restart" title="Restart" class="btn btn-sm btn-outline-dark action-button">`, 
                '<i class="fas fa-redo-alt"></i>',
                '</button>',
            ].join(' '); 
        }
            return btn ;
        }

        $(document).ready(function() {
            $('#table').on('post-body.bs.table', function (data) {
                console.log(data);
                setTimeout(function() {
                    $('.action-button').tooltip('hide').tooltip('dispose').tooltip({
                        placement: 'left',
                        container: 'body'
                    });
                }, 100);
                $('.action-button').off().on('click', function(event) {
                    $button = $(event.currentTarget);
                    var progressIcon = '<i class="fas fa-circle-notch fa-spin"></i>';
                    var originalIcon = $button.html();

                    var postData = {
                        vm: $button.data('vm'),
                        resource: $button.data('resource'),
                        action: $button.data('action'),
                    }

                    $button.html(progressIcon);
                    $button.attr('disabled', true);

                    axios.post("{{ route('azure.action') }}", postData)
                        .then(function (response) {
                            // handle success
                            console.log(response);
                            toastr.success(response.data.message, 'Action Completed');
                            $button.html(originalIcon);
                            $button.attr('disabled', false);
                        })
                        .catch(function (error) {
                            // handle error
                            toastr.error('Something went wrong.', 'Error Occured.');
                            $button.html(originalIcon);
                            $button.attr('disabled', false);
                        })

                });

                // '.action-button'
            });
        })
    
</script>

@endsection