@component('mail::message')
# Hello, {{ucfirst($trigger->wvd->last_user)}}

Your WVD <b>{{$trigger->wvd->vm_name}}</b> is scheduled to shutdown at <b>{{$shutdownAt}}</b>.
You can use the below buttons to postpone the shutdown.

{{-- {{dd($trigger->toArray())}} --}}

<div style="text-align: center">
<b>Postpone for</b>
</div>
<br/>
<div style="text-align: center">
<a href="{{config('azure.public_url')}}wvd/{{ $trigger->uid }}/postpone?hours=1" class="button button-blue">1 Hour</a>
<a href="{{config('azure.public_url')}}wvd/{{ $trigger->uid }}/postpone?hours=2" class="button button-blue">2 Hour</a>
<a href="{{config('azure.public_url')}}wvd/{{ $trigger->uid }}/postpone?hours=3" class="button button-blue">3 Hour</a>
<a href="{{config('azure.public_url')}}wvd/{{ $trigger->uid }}/postpone?days=1" class="button button-red">1 Day</a>
</div>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
