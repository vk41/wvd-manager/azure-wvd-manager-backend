@extends('layouts.app')
@section('title', 'Dashboard')

@section('content_header')
<h1 class="m-0 text-dark">Manage WVDs</h1>
@stop

@section('content')
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    {{-- <h3 class="card-title">DataTable with minimal features & hover style</h3> --}}
                    <a href="{{route("wvd.add")}}" class="btn btn-primary" href>Add New</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="wvds-table" class="table table-bordered table-hover"></table>
                </div>
            </div>
        </div>
    </div>
</section>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addNewModal">
    Launch demo modal
</button>

<div class="modal fade" id="addNewModal" tabindex="-1" role="dialog" aria-labelledby="addNewModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addNewModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="usernameInput">Username</label>
                        <input type="text" name="username" class="form-control" id="usernameInput"
                            placeholder="Ex: Vish"  value="{{ old('vm_name') }}">
                    </div>
                    <div class="form-group">
                        <label for="vmnameInput">VM Name</label>
                        <input type="text" name="vm_name" class="form-control" id="vmnameInput"
                            placeholder="Ex: AZ-IT-2" value="{{ old('vm_name') }}" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="resourceGroupInput">Resource Group</label>
                        <input type="text" name="resource_group" class="form-control" id="resourceGroupInput"
                            placeholder="Ex: AZ-WVDPOOL-RG"  value="{{ old('resource_group') }}">
                    </div>
                    <div class="form-group">
                        <label for="powerOffTimeInput">Power off at</label>
                        <input type="text" name="poweroff_at" class="form-control" id="powerOffTimeInput"
                            placeholder="Ex: 10:00PM"  value="{{ old('poweroff_at') }}">
                    </div>
                    <div class="form-group">
                        <label for="powerOnTimeInput">Power on at</label>
                        <input type="text" name="poweron_at" class="form-control" id="powerOnTimeInput"
                            placeholder="Ex: 10:00AM"  value="{{ old('poweron_at') }}">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready(function() {
    //     $("#example1").DataTable({
    //   "responsive": true,
    //   "autoWidth": false,
    // });
        var url = '{{route("wvd.data")}}';
        var packagesTable = $('#wvds-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            responsive: true,
            pageLength: 20,
            order: [[ 7, 'desc' ]],
            ajax: url,
            lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
            columns: [                
                {data: 'tracking_number', width: '200px',  title: 'Tracking No', render: function ( data, type, row ) {
                    return `<a target="_blank" class="details-modal" href="${row.package_url}">${data}</a>&nbsp;&nbsp;
                            <a class="btn btn-sm btn-primary float-right" target="_blank" rel="noreferrer" href="${row.carrier_url}"><i class="fa fa-external-link" aria-hidden="true"></i></a>`
                }},
                {data: 'master_tracking_number', width: '150px', title: 'Master'},
                {data: 'shipper_city', title: 'From', width: '150px'},
                {data: 'recipient_city', title: 'To', width: '150px'},
                {data: 'recipient_city', title: 'Time Elapsed', width: '90px', render: function ( data, type, row ) {
                    return `<span data-toggle="tooltip" data-placement="top" title="${row.shipped_on}">` + moment.utc(row.shipped_on).fromNow(true)+ '</span>';
                }},
                {data: 'current_location', title: 'Current Location'},
                {data: 'status', title: 'Status', width: '150px', render: function ( data, type, row ) {
                    if(row.completed) {
                        return `<span class="badge badge-success">${data}</span>`;
                    }else if (row.exception) {
                        return `<span class="badge badge-danger">${data}</span>`;
                    }else {
                        return `<span class="badge badge-warning">${data}</span>`;
                    }
                }},
                {data: 'exception', title: 'Exception',width: '80px', render: function ( data, type, row ) {
                    if(data == 0) {
                        return 'No';
                    }
                    var excepionMessage = row.exception_action == null || row.exception_action == '' ? '': row.exception_action;
                    var showStar = excepionMessage == '' ? '': '*';
                    return `<b data-toggle="tooltip" data-placement="top" title="${excepionMessage}">Yes${showStar}</b>`;
                }},
                {data: 'est_delivery',width: '150px', title: 'Est Delivery', render: function ( data, type, row ) {
                    return data == null || data == '' ? 'Pending': moment.utc(row.updated_at).format('YYYY-MM-DD');
                }},
                {data: 'updated_at',width: '150px', title : 'Last Updated', render: function ( data, type, row ) {
                    return `<span data-toggle="tooltip" data-placement="top" title="${row.last_status_updated}">` + moment.utc(row.last_status_updated).fromNow()+ '</span>';                    
                }},
            ],
            rowCallback: function(row, data, displayNum, displayIndex, dataIndex ) {
                $(row).find('.details-modal').off().on('click', openDetailsPane);
                if(data.completed) {
                    $(row).addClass('table-success');
                }
                if(data.exception) {
                    $(row).addClass('table-danger');
                }
            },
            drawCallback: function(settings) {
                $(settings.nTable).find('[data-toggle="tooltip"]').tooltip()
            }
        })
        .on('processing.dt', function (e, settings, processing) {            
            if (processing) {
                $('#table-overlay').removeClass("d-none");
            } else {
                $('#table-overlay').addClass("d-none", true);
            }
        });
    });

</script>

@endsection