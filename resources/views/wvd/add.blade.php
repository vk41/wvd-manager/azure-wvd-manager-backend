@extends('layouts.app')
@section('title', 'Dashboard')

@section('content_header')
<h1 class="m-0 text-dark">Add WVD</h1>
@stop

@section('content')
<section class="content">
    <div class="row">
        <div class="col-6 offset-md-3">
            <div class="card">
                <!-- /.card-header -->
                <form id="quickForm" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="usernameInput">Username</label>
                            <input type="text" name="username" class="form-control" id="usernameInput"
                                placeholder="Ex: Vish">
                        </div>
                        <div class="form-group">
                            <label for="vmnameInput">VM Name<span class="text-danger">*</span></label>
                            <input type="text" name="vm_name" class="form-control" id="vmnameInput"
                                placeholder="Ex: AZ-IT-2">
                        </div>
                        <div class="form-group">
                            <label for="resourceGroupInput">Resource Group<span class="text-danger">*</span></label>
                            <input type="text" name="resource_group" class="form-control" id="resourceGroupInput"
                                placeholder="Ex: AZ-WVDPOOL-RG">
                        </div>
                        <div class="row">
                            <div class="form-group date col-6">
                                <label for="powerOffTimeInput">Power off at<span class="text-danger">*</span></label>
                                <input type="text" name="poweroff_at" class="form-control datetimepicker-input"
                                    id="powerOffTimeInput" data-toggle="datetimepicker" data-target="#powerOffTimeInput"
                                    placeholder="Ex: 10:00PM">
                            </div>
                            <div class="form-group date col-6">
                                <label for="powerOnTimeInput">Power on at<span class="text-danger">*</span></label>
                                <input type="text" name="poweron_at" class="form-control datetimepicker-input"
                                    id="powerOnTimeInput" data-toggle="datetimepicker" data-target="#powerOnTimeInput"
                                    placeholder="Ex: 10:00AM">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer clearfix">
                        <button type="submit" class="btn btn-info float-right"><i class="fas fa-plus"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('styles')
<link href="{{ asset('plugins/bootstrap-datetimepicker/css/tempusdominus-bootstrap-4.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{ asset('plugins/bootstrap-datetimepicker/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
      $.validator.setDefaults({
        
      });

      $('#quickForm').validate({
        submitHandler: function (form) {
            $(form).submit();
        },
        rules: {
          vm_name: {
            required: true,
          },
          resource_group: {
            required: true,
          },
          poweroff_at: {
            required: true
          },
          poweron_at: {
            required: true
          },
        },
        messages: {
          vm_name: 'VM name is required.',
          resource_group: 'Resource Group is required.',
          poweroff_at: 'Power off time is required.',
          poweron_at: 'Power on time is required.',
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });

        $('#powerOffTimeInput, #powerOnTimeInput').datetimepicker({
            format: 'LT'
        });
    });
</script>

@endsection