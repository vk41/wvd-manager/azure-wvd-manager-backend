<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Wvd;
use App\Models\Trigger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\ShutdownNotificationMail;
use App\Events\RefreshTriggerDataEvent;

class TriggerController extends Controller
{
    public function index($vmName, Request $request)
    {
        $wvd = Wvd::where('vm_name', $vmName)->first();
        if (!$wvd) {
            return response()->json(['success' => false, 'message' => 'Item Not found'], 422);
        }

        $wvd->setLastUser($request->last_user)->save();
        $shutdownTrigger = Trigger::where('vm_name', $wvd->vm_name)->whereIn('action', ['deallocate', 'stop'])->first();
        $response = [
            'success' => true,
            'message' => 'Registered',
            'data' => $shutdownTrigger
        ];
        return response()->json($response);
    }

    public function postpone($uid, Request $request)
    {

        $shutdownTrigger = Trigger::where('uid', $uid)->first();
        if (!$shutdownTrigger) {
            return response()->json(['success' => false, 'message' => 'Item Not found'], 422);
        }
        $currentTriggerTime = Carbon::parse($shutdownTrigger->trigger_at);
        $changeTriggerTo = $currentTriggerTime->addHours($request->hours);
        $message = 'Postponed to for ' . $request->hours . ' hours.';
        $shutdownTrigger->setRespose($message)->setTriggerTime($changeTriggerTo)->save();
        $shutdownTrigger->save();
        $this->sendTriggerMail($shutdownTrigger);
        event(new RefreshTriggerDataEvent($shutdownTrigger));
        // return $shutdownTrigger;

        $response = [
            'success' => true,
            'message' => $message,
            'data' => $shutdownTrigger
        ];
        return response()->json($response);
    }

    public function confirm($uid, Request $request)
    {
        $shutdownTrigger = Trigger::where('uid', $uid)->first();
        if (!$shutdownTrigger) {
            return response()->json(['success' => false, 'message' => 'Item Not found'], 422);
        }
        $shutdownTrigger->setRespose('Confirmed')->save();
        
        $response = [
            'success' => true,
            'message' => 'Confirmed',
            'data' => $shutdownTrigger
        ];
        return response()->json($response);
    }

    public function skip($uid, Request $request)
    {
        $shutdownTrigger = Trigger::where('uid', $uid)->first();
        if (!$shutdownTrigger) {
            return response()->json(['success' => false, 'message' => 'Item Not found'], 422);
        }
        $currentTriggerTime = Carbon::parse($shutdownTrigger->trigger_at);
        $changeTriggerTo = $currentTriggerTime->addDay();
        $shutdownTrigger->setRespose('Skiped')->setTriggerTime($changeTriggerTo)->save();
        $this->sendTriggerMail($shutdownTrigger);
        event(new RefreshTriggerDataEvent($shutdownTrigger));
        $response = [
            'success' => true,
            'message' => 'Skiped',
            'data' => $shutdownTrigger
        ];
        return response()->json($response);
    }

    public function reset($uid, Request $request)
    {
        $shutdownTrigger = Trigger::where('uid', $uid)->first();
        if (!$shutdownTrigger) {
            return response()->json(['success' => false, 'message' => 'Item Not found'], 422);
        }
        $wvd = Wvd::where('vm_name', $shutdownTrigger->vm_name)->first();
        $changeTriggerTo =  Carbon::parse(Carbon::createFromFormat('H:i:s', $wvd->poweroff_at)->format('H:i'));
        $shutdownTrigger->setRespose('Resetted')->setTriggerTime($changeTriggerTo)->save();
        
        $response = [
            'success' => true,
            'message' => 'Skiped',
            'data' => $shutdownTrigger
        ];
        return response()->json($response);
    }

    protected function sendTriggerMail($trigger)
    {
        
        try {
            $userEmail = $trigger->wvd->last_user . '@aquacom.com';
            // dd("Queuing mail to $userEmail");
            Log::info("Queuing mail to $userEmail");
            Mail::to($userEmail)->queue(new ShutdownNotificationMail($trigger));
            return true;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
    }

    // public function mail($uid){
    //     $shutdownTrigger = Trigger::with('wvd')->where('uid', $uid)->first();
    //     return (new \App\Mail\ShutdownNotificationMail($shutdownTrigger))->render();
    // }

}
