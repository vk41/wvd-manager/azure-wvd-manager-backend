<?php

namespace App\Http\Controllers;

use App\Models\Wvd;
use App\Library\Azure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AzureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $users = $request->input('users', []);

        $wvd = Wvd::whereIn('last_user', $users)->get();
        if ($wvd->count() == 0) {
            Log::debug(json_encode($request->wvds));
            return response()->json(['success' => false, 'message' => 'Items Not found'], 422);
        }

        $response = ['success' => true, 'data' => $wvd];
        return response()->json($response);
    }

    public function data()
    {
        return (new Azure)->getList();
    }

    public function action($vm_name, Request $request)
    {
        // if (!in_array($vm_name, ['AZ-IT-2', 'AZ-PURCHSNG-8'])) {
        //     return response()->json(['success' => false, 'message' => 'Not Allowed.'], 422); // Test mode
        // }

        $wvd = Wvd::where('vm_name', $vm_name)->first();
        if (!$wvd) {
            return response()->json(['success' => false, 'message' => 'Item Not found'], 422);
        }
        $action = $request->action;

        try {
            $response = ['success' => true];
            switch ($action) {
                case 'deallocate':
                    $response['data'] = (new Azure)->deallocate($wvd->resource_group, $wvd->vm_name);
                    break;
                case 'start':
                    $response['data'] = (new Azure)->powerOn($wvd->resource_group, $wvd->vm_name);
                    break;
                case 'restart':
                    $response['data'] = (new Azure)->restart($wvd->resource_group, $wvd->vm_name);
                    break;
                default:
                    return response()->json(['success' => false, 'message' => "Action $action Not found."], 422);
                    break;
            }
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->json(['success' => false, 'message' => 'Something went wrong.'], 400);
        }
    }
}
