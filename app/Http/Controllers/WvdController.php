<?php

namespace App\Http\Controllers;

use App\Models\Wvd;
use Illuminate\Http\Request;

class WvdController extends Controller
{
    public function index() {
        return view('wvd.index');
    }

    public function data(Request $request)
    {
        $wvds = Wvd::all();       
        return \Yajra\DataTables\Facades\DataTables::of($wvds)->make(true);
    }

    public function add() {
        return view('wvd.add');
    }
    
    public function addConfirm(Request $request) {
        
        return $request->all();
    }
}
