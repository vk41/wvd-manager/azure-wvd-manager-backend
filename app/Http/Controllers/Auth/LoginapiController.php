<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
// use App\Providers\RouteServiceProvider;
use Illuminate\Validation\ValidationException;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginapiController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */    

    public function login(Request $request) {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
            'device_name' => 'required',
        ]);
    
        $user = User::where('username', $request->username)->first();
        // dd(Hash::make($request->password), $user->password);
    
        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'username' => ['The provided credentials are incorrect.'],
            ]);
        }
        $token = $user->createToken($request->device_name)->plainTextToken;

        return response()->json([
            'success' => true,
            'token' => $token
        ]);
    }

    public function check(Request $request) {
        $user = $request->user()->only(['username', 'full_name','email']);
        return response()->json(['success' => true, 'data' => $user]);
    }

    public function logout(Request $request) {
        \Illuminate\Support\Facades\Storage::put('Wvd.json', json_encode($request->user));
        $request->user()->currentAccessToken()->delete();
        return response()->json(['success' => true]);
        // return response()->json($request->user()->currentAccessToken());
    }
}
