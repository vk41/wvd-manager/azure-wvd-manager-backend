<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Trigger;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Events\ShutDownNotification;
use Illuminate\Support\Facades\Mail;
use App\Mail\ShutdownNotificationMail;
use phpDocumentor\Reflection\Types\Boolean;

class NotifyTriggers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'triggers:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notifications to user about shutdown.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $notifycationTime = config('azure.notification.before');

        $oneHourTriggerTime = [
            Carbon::parse(Carbon::now()->addMinutes(59)->format('H:i'))->toDateTimeString(),
            Carbon::parse(Carbon::now()->addMinutes(61)->format('H:i'))->toDateTimeString()
        ];

        $tenMinuteTriggerTime = [
            Carbon::parse(Carbon::now()->addMinutes(9)->format('H:i'))->toDateTimeString(),
            Carbon::parse(Carbon::now()->addMinutes(11)->format('H:i'))->toDateTimeString()
        ];

        $notificationTime = [
            Carbon::parse(Carbon::now()->subMinutes(2)->format('H:i'))->toDateTimeString(),
            Carbon::parse(Carbon::now()->addMinutes(2)->format('H:i'))->toDateTimeString()
        ];

        $triggers = Trigger::with('wvd')->whereIn('action', ['deallocate', 'stop'])
            ->where(function ($query) use ($oneHourTriggerTime, $tenMinuteTriggerTime) {
                $query->whereBetween('trigger_at', $oneHourTriggerTime)->orWhereBetween('trigger_at', $tenMinuteTriggerTime);
            })
            ->whereNotBetween('notified_at', $notificationTime)
            ->get();

        $output = [];
        foreach ($triggers as $trigger) {
            print_r($trigger->notified_at);

            $emailSent = $this->sendMail($trigger);
            $wsSent = $this->sendWs($trigger);
            $output[] = [$trigger->vm_name, $trigger->trigger_at, $trigger->action, (bool) $wsSent, (bool) $emailSent];
            $trigger->setNotified(Carbon::now())->save();
        }

        $this->line('');
        if (sizeof($output) > 0) {
            $this->line('============ From notifications ============');
            $this->table(['WVD', 'Time', 'Action', 'WS sent', 'Email sent'], $output);
        } else {
            $this->line('============ No notifications send ============');
        }
    }

    protected function sendWs(Trigger $trigger)
    {
        try {
            event(new ShutDownNotification($trigger));
            return true;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
        return false;
    }
    protected function sendMail(Trigger $trigger)
    {

        try {
            $userEmail = $trigger->wvd->last_user . '@aquacom.com';
            Mail::to($userEmail)->queue(new ShutdownNotificationMail($trigger));
            return true;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
        return false;
    }
}
