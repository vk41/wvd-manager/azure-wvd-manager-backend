<?php

namespace App\Console\Commands;

use App\Library\Azure;
use Carbon\Carbon;
use App\Models\Trigger;
use App\Models\TriggerLog;
use Illuminate\Console\Command;
use App\Jobs\TriggerJob;

class RunTriggers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'triggers:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run scheduled triggers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        $triggerTime =[
            Carbon::parse(Carbon::now()->subMinutes(2)->format('H:i'))->toDateTimeString(),
            Carbon::parse(Carbon::now()->format('H:i'))->toDateTimeString()
        ];
        

        $triggers = Trigger::whereBetween('trigger_at',  $triggerTime)->get();
        $output = [];
        foreach($triggers as $trigger) {
            TriggerJob::dispatch($trigger);
            $output[] = [$trigger->vm_name, $trigger->trigger_at, $trigger->action];
        }
        
        $this->line('');
        if(sizeof($output)>0) {
        $this->line('============ From Run triggers ============');
            $this->table(['WVD', 'Time', 'Action'], $output);
        }else {
            $this->line('============ No triggers to run ===============');
        }
        
    }
}
