<?php

namespace App\Console\Commands;

use App\Models\Wvd;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class AllTriggers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'triggers:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $notifycationTime = config('azure.notification.before');

        $timeTable = [];
        $timeTable[] = [ 'Now', Carbon::now()->format('H:i:s'), 'For ref'];
        $timeTable[] = [ 'Add '.$notifycationTime.' Min ', Carbon::parse(Carbon::now()->addMinutes($notifycationTime)->format('H:i'))->toDateTimeString(), 'In add and notify trigger, as to time'];
        
        


        $this->table(['When', 'Time', 'Used in'], $timeTable);
        $output = '';
        // Artisan::call('triggers:add', [], $this->getOutput());
        Artisan::call('triggers:notify', [], $this->getOutput());
        Artisan::call('triggers:run', [], $this->getOutput());
    }
}
