<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Library\Azure;
use App\Models\Trigger;
use Illuminate\Console\Command;
use App\Events\ShutDownNotification;
use App\Models\Wvd;
use Illuminate\Support\Facades\Storage;

class TestTrigger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'triggers:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $wvds = Wvd::all();
        $csv = [];
        $csv[] = implode(',', ['user', 'start', 'stop', 'timezone']);
        foreach($wvds as $wvd) {
            $csv[] = implode(',', [
                $wvd->last_user,
                Carbon::parse(Carbon::createFromFormat('H:i:s', $wvd->poweron_at)->setTimezone($wvd->timezone))->format('h:ia'),
                Carbon::parse(Carbon::createFromFormat('H:i:s', $wvd->poweroff_at)->setTimezone($wvd->timezone))->format('h:ia'),
                $wvd->timezone
            ])  ;          
        }
        Storage::put('wvd-timers.csv', implode("\r\n", $csv));

        // $wvds = (new Azure)->getList();
        // $csv = [];
        // $csv[] = implode(',', array_keys($wvds[0]));
        // foreach($wvds as $wvd) {
        //     $csv[] = implode(',',array_values($wvd));
        // }

        // Storage::put('allWvds.csv', implode("\r\n", $csv));
        // Storage::put('allWvds.json', json_encode($wvds));
        // $notifycationTime = config('azure.notification.before');

        // $timeTable = [];
        // $timeTable[] = [ 'Now', Carbon::now()->format('H:i:s'), 'For ref'];
        // $timeTable[] = [ 'Add  Min ', Carbon::parse(Carbon::now()->addMinutes($notifycationTime)->format('H:i'))->toDateTimeString(), 'In add and notify trigger, as to time'];
        


        // // $this->table(['When', 'Time', 'Used in'], $timeTable);

        // $trigger = Trigger::first();
        // \App\Jobs\TriggerJob::dispatch($trigger);
    }
}
