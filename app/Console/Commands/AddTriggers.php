<?php

namespace App\Console\Commands;

use App\Models\Trigger;
use App\Models\Wvd;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AddTriggers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'triggers:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add jobs to trigger table to run later.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $output = [];
        $wvds = Wvd::active()->get();
        // dd($wvds->toArray());
        $dayOfTheWeek = Carbon::now()->format('w');
        foreach ($wvds as $eachWvd) {
            $addOffResponse = $eachWvd->addOffTrigger();
            $output[] = [$eachWvd->vm_name, $addOffResponse->trigger_at, 'deallocate'];
            if (!in_array($dayOfTheWeek, $eachWvd->daysofweek)) {
                // $output[] = [$eachWvd->vm_name, $eachWvd->poweron_at, $eachWvd->poweroff_at, 'Skip for the day'];
                continue;
            }
            
            $addOnResponse = $eachWvd->addOnTrigger();
            $output[] = [$eachWvd->vm_name, $addOnResponse->trigger_at, 'start'];       
        }

        

        $this->line('');
        if (sizeof($output) > 0) {
            $this->line('============ From Add Triggers ============');
            $this->table(['WVD', 'TriggerTime',  'Action'], $output);
        } else {
            $this->line('============ No triggers to add ===============');
        }
    }
}
