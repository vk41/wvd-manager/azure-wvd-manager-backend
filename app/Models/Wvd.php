<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Trigger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Wvd extends Model
{

    protected $casts = [
        'daysofweek' => 'array',
    ];

    public function scopeActive($query)
    {
        return $query->where('autostart', 1);
    }

    public function triggers()
    {
        return $this->hasMany(Trigger::class, 'vm_name', 'vm_name');
    }

    public function addOnTrigger()
    {
        
        $startupTime =  Carbon::parse(Carbon::createFromFormat('H:i:s', $this->poweron_at, $this->timezone)->setTimezone('UTC')->format('H:i'));
        if($startupTime->isPast()) {
            $startupTime = $startupTime->addDay();  
        }

        return Trigger::updateOrCreate(
            [
                'vm_name' => $this->vm_name,
                'action' => 'start'
            ],
            [
                'resource_group' => $this->resource_group,
                'trigger_at' => $startupTime,
                'notified_at' => Carbon::now() // Important
            ]
        );
    }
    public function addOffTrigger()
    {
        $shutdownTime =  Carbon::parse(Carbon::createFromFormat('H:i:s', $this->poweroff_at, $this->timezone)->setTimezone('UTC')->format('H:i'));
        if($shutdownTime->isPast()) {
            $shutdownTime = $shutdownTime->addDay();  
        }
        

        return Trigger::firstOrCreate(
            [
                'vm_name' => $this->vm_name,
                'action' => 'deallocate'
            ],
            [
                'resource_group' => $this->resource_group,
                'trigger_at' => $shutdownTime,
                'notified_at' => Carbon::now()
            ]
        );
    }



    public function setLastUser($username)
    {
        $this->last_user = $username;
        // $this->save();
        return $this;
    }

    // public function getPoweroffTriggerAtAttribute()
    // {
    //     $trigger = $this->triggers->whereIn('action', ['deallocate', 'stop'])->first();
    //     return $trigger->trigger_at;
    // }

    // public function getPoweronTriggerAtAttribute()
    // {
    //     $trigger = $this->triggers->firstWhere('action', 'start');
    //     return $trigger->trigger_at;
    // }
}
