<?php
namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use HasApiTokens;
    protected $table = 'portaldb_test.acl_users';
    protected $primaryKey = 'username';
    protected $keyType = 'string';

    public function getKeyName()
    {
        return 'username';
    }
}
