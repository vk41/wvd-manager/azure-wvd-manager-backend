<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Wvd;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Trigger extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'uid';
    protected $keyType = 'string';
    
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($trigger) {
            $trigger->uid = (string) Str::uuid();
        });
    }

    public function wvd()
    {
        return $this->hasOne(Wvd::class, 'vm_name', 'vm_name');
    }

    public function archive() {
        $log = TriggerLog::create($this->toArray());
        $this->delete();
    }

    public function setNotified(Carbon $time = null) {
        $this->notified_at = $time;
        return $this;        
    }
    
    public function setTriggerTime(Carbon $time = null) {
        $this->trigger_at = $time;
        return $this;        
    }
    public function setCompleted(Carbon $time = null) {
        $this->completed_at = $time;
        return $this;        
    }

    // public function setResponded(Carbon $time = null) {
    //     $this->responded_at = $time;
    //     return $this;     
    // }

    public function setRespose($response) {
        $this->responded_at = Carbon::now();
        $this->response = $response;
        return $this;     
    }
    
}
