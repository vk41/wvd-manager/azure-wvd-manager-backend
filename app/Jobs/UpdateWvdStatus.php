<?php

namespace App\Jobs;

use App\Models\Wvd;
use App\Library\Azure;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateWvdStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $wvds = Wvd::all();
        $statuses = (new Azure)->updateWvdStatus();
        // $statuses = collect($statuses)->keyBy('virtualMachine');
        // foreach($wvds as $wvd) {
        //     if(isset( $statuses[$wvd->vm_name])) {
        //         $wvd->state = $statuses[$wvd->vm_name]['status'];
        //         $wvd->save();
        //     }
        // }
    }
}
