<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Trigger;
use App\Library\Azure;
use Carbon\Carbon;

class TriggerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $trigger;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Trigger $trigger)
    {
        $this->trigger = $trigger;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Azure::trigger($this->trigger);
        $this->trigger->setCompleted(Carbon::now())->save();
        $this->trigger->archive();
    }
}
