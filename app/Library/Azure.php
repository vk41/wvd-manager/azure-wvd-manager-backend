<?php

namespace App\Library;

use App\Models\Trigger;
use App\Models\Wvd;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Azure
{

    protected $token;
    protected $tenantId;
    protected $clientId;
    protected $clientSecret;
    protected $subscriptionId;
    protected $client;

    public function __construct()
    {
        $this->tenantId = config('azure.tenant_id');
        $this->clientId = config('azure.client_id');
        $this->clientSecret = config('azure.client_secret');
        $this->subscriptionId = config('azure.subscription_id');
        $this->token = cache('azureToken');
        if ($this->token == null) {
            $this->token = $this->getAuth();
        }

        $this->client = new Client([
            'base_uri' => 'https://management.azure.com',
            'headers' => ['Authorization' => 'Bearer ' . $this->token],
        ]);
    }

    protected function getAuth()
    {
        $client = new Client();
        $tenantId = $this->tenantId;
        $url = "https://login.microsoftonline.com/{$tenantId}/oauth2/token";
        $response = $client->request('POST', $url, [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'tenant_id' => $this->tenantId,
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'resource' => 'https://management.azure.com/',
            ]
        ]);
        $body = $response->getBody();
        $responseData = json_decode($body->getContents());
        Cache::put('azureToken', $responseData->access_token, now()->addMinutes(59));
        $this->token = $responseData->access_token;
        return $responseData->access_token;
    }

    public function getWvd(Wvd $wvd)
    {
        $subscriptionId = $this->subscriptionId;
        $client = $this->client;
        $url = '\/subscriptions/' . $subscriptionId . '\/resourceGroups/' . $wvd->resource_group . '/providers/Microsoft.Compute/virtualMachines/' . $wvd->vm_name . '?api-version=2019-12-01&$expand=instanceView';
        $response = $client->get($url);
        $body = $response->getBody();
        $responseData = json_decode($body->getContents());
        return $this->formatVMData($responseData);
    }

    public function getList()
    {
        if (cache('testData')) {
            // return cache('testData');
        }
        $subscriptionId = $this->subscriptionId;
        $client = $this->client;
        $url = "/subscriptions/{$subscriptionId}/providers/Microsoft.Compute/virtualMachines?api-version=2019-12-01&statusOnly=true";
        $list = [];
        $wholeList = [];
        while ($url) {
            $response = $client->get($url);
            $body = $response->getBody();
            $responseData = json_decode($body->getContents());
            foreach ($responseData->value as $eachWvd) {
                $wholeList[] = $eachWvd;
                $list[] = $this->formatVMData($eachWvd);
            }
            if (isset($responseData->nextLink)) {
                $url = $responseData->nextLink;
            } else {
                $url = false;
            }
        }
        // \Illuminate\Support\Facades\Storage::put('wholeWvd.json', json_encode($wholeList));
        Cache::put('testData', $list, now()->addMinutes(5));
        return $list;
    }

    protected function formatVMData($wvd)
    {
        $id = substr($wvd->id, 1);
        $vmInfo = explode('/', $id);
        $resourceInformation = [];

        foreach ($vmInfo as $key => $value) {
            if ($key % 2 == 0) {
                $resourceInformation[$value] = $vmInfo[$key + 1];
            }
        }
        $serviceStatus = null;
        try {
            $serviceStatus = end($wvd->properties->instanceView->statuses)->code;
            $serviceStatus = explode('/', $serviceStatus);
            $serviceStatus = end($serviceStatus);
        } catch (\Exception $e) {
            Log::alert($e->getMessage());
        }

        return [
            'virtualMachine' => $resourceInformation['virtualMachines'],
            'resourceGroup' => $resourceInformation['resourceGroups'],
            'status' => $serviceStatus
        ];
    }

    public static function trigger(Trigger $trigger)
    {
        return (new Azure())->runTrigger($trigger);
    }

    public function runTrigger(Trigger $trigger)
    {

        try {
            switch ($trigger->action) {
                case 'deallocate':
                    $this->deallocate($trigger->resource_group, $trigger->vm_name);
                    break;

                case 'start':
                    $this->powerOn($trigger->resource_group, $trigger->vm_name);
                    break;

                case 'stop':
                    $this->powerOff($trigger->resource_group, $trigger->vm_name);
                    break;

                case 'restart':
                    $this->restart($trigger->resource_group, $trigger->vm_name);
                    break;
            }

            return [
                'resource' => $trigger->resource_group,
                'vm' => $trigger->vm_name,
                'action' => $trigger->action,
                'success' => true,
                'message' => "{$trigger->vm_name} power on request sent.",
            ];
        } catch (\Exception $err) {
            throw $err;
        }
    }

    /**
     * Power on Virtual Machine
     *
     * @param string $resource
     * @param string $vm
     * @return array
     */
    public function powerOn($resource, $vm)
    {
        $subscriptionId = $this->subscriptionId;
        $url = "/subscriptions/{$subscriptionId}/resourceGroups/{$resource}/providers/Microsoft.Compute/virtualMachines/{$vm}/start?api-version=2019-12-01";
        $client = $this->client;
        try {
            $response = $client->post($url);
            $body = $response->getBody();
            return ['action' => 'powerOn', 'vm' => $vm, 'response' => json_decode($body->getContents())];
        } catch (\Exception $err) {
            throw $err;
        }
    }

    /**
     * Deallocate Virtual Machine
     *
     * @param string $resource
     * @param string $vm
     * @return array
     */
    public function powerOff($resource, $vm)
    {
        $subscriptionId = $this->subscriptionId;
        $url = "/subscriptions/{$subscriptionId}/resourceGroups/{$resource}/providers/Microsoft.Compute/virtualMachines/{$vm}/powerOff?api-version=2019-12-01";
        $client = $this->client;
        try {
            $response = $client->post($url);
            $body = $response->getBody();
            return ['action' => 'powerOff', 'vm' => $vm, 'response' => json_decode($body->getContents())];
        } catch (\Exception $err) {
            throw $err;
        }
    }

    /**
     * Deallocate Virtual Machine
     *
     * @param string $resource
     * @param string $vm
     * @return array
     */
    public function deallocate($resource, $vm)
    {
        $subscriptionId = $this->subscriptionId;
        $url = "/subscriptions/{$subscriptionId}/resourceGroups/{$resource}/providers/Microsoft.Compute/virtualMachines/{$vm}/deallocate?api-version=2019-12-01";
        $client = $this->client;
        try {
            $response = $client->post($url);
            $body = $response->getBody();
            return ['action' => 'deallocate', 'vm' => $vm, 'response' => json_decode($body->getContents())];
        } catch (\Exception $err) {
            throw $err;
        }
    }

    public function restart($resource, $vm)
    {
        $subscriptionId = $this->subscriptionId;
        $url = "/subscriptions/{$subscriptionId}/resourceGroups/{$resource}/providers/Microsoft.Compute/virtualMachines/{$vm}/restart?api-version=2019-12-01";
        $client = $this->client;
        try {
            return $client->post($url);
            $response = $client->post($url);
            $body = $response->getBody();
            return ['action' => 'restart', 'vm' => $vm, 'response' => json_decode($body->getContents())];
        } catch (\Exception $err) {
            throw $err;
        }
    }
}
