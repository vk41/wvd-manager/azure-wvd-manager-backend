<?php

namespace App\Events;

use App\Models\Trigger;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class ShutDownNotification implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $trigger;
    public $shutdownTime;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Trigger $trigger)
    {
        $this->trigger = $trigger;
        $this->shutdownTime = $trigger->trigger_at;

        // 'Your PC is scheduled to Shutdown at 10:00 PM every day. And will auto start at 8:30AM next day.'
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('wvd.' . strtolower($this->trigger->vm_name));
    }

}
