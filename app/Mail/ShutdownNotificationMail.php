<?php

namespace App\Mail;

use Carbon\Carbon;
use App\Models\Trigger;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShutdownNotificationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $trigger;
    public $shutdownAt;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Trigger $trigger)
    {
        $this->trigger = $trigger;
        $this->shutdownAt = Carbon::parse($this->trigger->trigger_at)->setTimezone($this->trigger->wvd->timezone)->format('d-M-Y h:ia');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.shutdown');
    }
}
