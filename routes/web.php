<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//   return redirect()->route('wvd.index');
// });

Auth::routes([
  'register' => false,
  'reset' => false,
  'verify' => false,
]);

Route::group(['prefix' => 'wvd', 'as' => 'wvd.', 'middleware' => 'auth'], function () {
  Route::get('/', 'WvdController@index')->name('index');
  Route::get('/data', 'WvdController@data')->name('data');
  Route::get('/add', 'WvdController@add')->name('add');
  Route::post('/add', 'WvdController@addConfirm')->name('addConfirm');
});

// Route::group(['prefix' => 'trigger/{uid}', 'as' => 'trigger.'], function () {
//   require 'triggerRoutes.php';
// });

// Route::get('/azure', 'AzureController@index')->name('azure');
// Route::get('/azure/data', 'AzureController@data')->name('azure.data');
// Route::post('/azure/action', 'AzureController@action')->name('azure.action');
