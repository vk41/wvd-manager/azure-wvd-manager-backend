<?php 
// Not used
use Illuminate\Support\Facades\Route;



  Route::post('/', 'TriggerController@index')->name('index');
    Route::post('/postpone', 'TriggerController@postpone')->name('postpone');
    Route::post('/confirm', 'TriggerController@confirm')->name('confirm');
    Route::post('/skip', 'TriggerController@skip')->name('skip');
    Route::post('/reset', 'TriggerController@reset')->name('reset');
    Route::get('/mail', 'TriggerController@mail')->name('mail');