<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Broadcast;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'user', 'as' => 'api.user.'], function () {
  Route::post('/login', 'Auth\LoginapiController@login')->name('login');
  Route::post('/logout', 'Auth\LoginapiController@logout')->name('logout')->middleware('auth:sanctum');
  Route::get('/check', 'Auth\LoginapiController@check')->name('check')->middleware('auth:sanctum');
});
Broadcast::routes(['middleware' => ['auth:sanctum']]);

Route::group(['prefix' => 'trigger/{uid}', 'as' => 'api.trigger.'], function () {
  Route::post('/', 'TriggerController@index')->name('index');
  Route::post('/postpone', 'TriggerController@postpone')->name('postpone');
  Route::post('/confirm', 'TriggerController@confirm')->name('confirm');
  Route::post('/skip', 'TriggerController@skip')->name('skip');
  Route::post('/reset', 'TriggerController@reset')->name('reset');
  Route::get('/mail', 'TriggerController@mail')->name('mail');
});
Route::group(['prefix' => 'azure', 'as' => 'api.azure.'], function () {
  Route::post('/', 'AzureController@index')->name('index');
  Route::post('/{vm_name}/action', 'AzureController@action')->name('action');
});
